@extends('layouts.app')
@section('content')

<body style="background-color:lightgray;">

<h1>Add a New Customer to the list</h1>


<form method = 'POST' action = "{{action('CustomerController@store')}}">

@csrf

<div class = "form-group">
<label for = "title">Customer Full Name</label>
<input type = "text" class= "form-control" name = "name">
</div>

<div class = "form-group">
<label for = "title">Customer Email Address</label>
<input type = "text" class= "form-control" name = "email">
</div>

<div class = "form-group">
<label for = "title">Customer Mobile Phone</label>
<input type = "text" class= "form-control" name = "phone">
</div>

<div class = "form-group">
<input type = "submit" class= "form-control" name = "submit">
</div>
</form>


@endsection

</body>