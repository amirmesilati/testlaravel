<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;

use App\Customer;
use App\User;


class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $id = Auth::id();
        $user = User::Find($id);
        $customers = Customer::All();
        return view('customers.index', ['customers' => $customers],['userid' => $id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer();
        $id = Auth::id(); //the id of the current user
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->user_id = $id;
        $customer->save();
        return redirect('customers');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userid = Auth::id();
        $customer = Customer::find($id);
        if (Gate::denies('manager')) {
            if($customer->user_id != $userid)
            abort(403,"Sorry, you do not hold permission to edit this customer");
        }
        return view('customers.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //only if this todo belongs to user
        $customer = Customer::find($id);
       //employees are not allowed to change the title 
    /*    if (Gate::denies('manager')) {
            if ($request->has('title'))
                   abort(403,"You are not allowed to edit todos..");
        }   
    */
        //make sure the todo belongs to the logged in user
   //     if(!$task->user->id == Auth::id()) return(redirect('tasks'));
   
        //test if title is dirty
       
        $customer->update($request->except(['_token'])); 
        return redirect('customers');  
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {    
        if (Gate::denies('manager')) {
        abort(403,"You are not allowed to delete customers");
   }
        $customer = Customer::find($id);
        $customer->delete();
        return redirect('customers');
    }


    public function close_deal($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"You are not allowed to close deals");
       }
        $customer = Customer::find($id);
        $customer->status=1;
        $customer->update();
        return redirect('customers');   
    }
}